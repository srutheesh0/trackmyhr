<?php

namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;

use App\Models\Hrtracker;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class HrtrackerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendhrdata(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'device_id' => 'required',
            'heartrate' => 'required',
            'lattitude' => 'required',
            'longitude' => 'required',
            'timevalue' => 'required'
        ]);

        $hrtracker = new Hrtracker();
        $hrtracker->user_id = $request->user_id;
        $hrtracker->device_id = $request->device_id;
        $hrtracker->heartrate = $request->heartrate;
        $hrtracker->lattitude = number_format($request->lattitude, 5);
        $hrtracker->longitude = number_format($request->longitude, 5);
        $hrtracker->timevalue = $request->timevalue;

        $response['user_id'] = (int)$hrtracker->user_id;
        $response['device_id'] = $hrtracker->device_id;
        $response['heartrate'] = $request->heartrate;
        $response['lattitude'] = number_format($hrtracker->lattitude, 5);
        $response['longitude'] = number_format($hrtracker->longitude, 5);
        $response['timevalue'] = $request->timevalue;
        $response['hrtimestamp'] = date($request->timevalue,'Y-m-d H:i:s');

        // print_r(auth()->user()->id);exit;
        // if ($hrtracker->save())
        if ($hrtracker->save())
            return response()->json([
                'status' => true,
                'data' => $response,
                'message' => 'Data saved successfully'
            ]);
        else
            return response()->json([
                'status' => false,
                'message' => 'Data could not Saved'
            ], 200);
    }



    public function gethrdata(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required',
        ]);
        $hrtracker = Hrtracker::select('user_id', 'device_id', 'heartrate', 'lattitude', 'longitude', 'timevalue')->where('user_id', $request->user_id)->orderby('id', 'DESC')->get();
        //$hrtracker = auth()->user()->hrtracker;
        // print_r($hrtracker->lattitude);

        //$hrtracker->lattitude=number_format($hrtracker->lattitude,5);
        //$hrtracker->longitude=number_format($hrtracker->longitude,5);
        return response()->json([
            'status' => true,
            'data' => $hrtracker,
            'message' => 'Data retrieved successfully'
        ], 200);
    }


    /* getuserlist */
    public function getuserlist()
    {
        $user = User::select('id as user_id', 'first_name', 'last_name')->get();
        if (!$user->isEmpty()) {
            return response()->json([
                'status' => true,
                'data' => $user,
                'message' => 'Data retrieved successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => true,
                'data' => "No Record Found",
                'message' => 'Data retrieved successfully'
            ], 200);
        }
    }

    /* getuserdata */

    public function getuserdata(Request $request)
    {
        $user = User::leftjoin('hrtrackers as h', 'users.id', '=', 'h.user_id')->where('users.id', '=', $request->user_id)->select('users.id as user_id', 'users.first_name', 'users.last_name', 'h.lattitude', 'h.longitude', 'h.device_id', 'h.heartrate', 'h.timevalue')->orderby('h.id', 'DESC')->first();
        if ($user) {
            return response()->json([
                'status' => true,
                'data' => $user,
                'message' => 'Data retrieved successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => true,
                'data' => [], //"No record Found",
                'message' => 'Data retrieved successfully'
            ], 200);
        }
    }


    /* getuserdetails  */

    public function getuserdetails(Request $request)
    {
        // $user=User::select('users.id as user_id','users.first_name','users.last_name','h.lattitude','h.longitude','h.device_id','h.heartrate','h.timevalue')
        //             ->leftjoin('hrtrackers as h','users.id','=','h.user_id')
        //             ->groupBy('h.user_id')
        //             ->orderBy('users.first_name','ASC')->orderBy('h.id','DESC')->get();


        $user = User::select('users.id as user_id', 'users.first_name', 'users.last_name', 'h.lattitude', 'h.longitude', 'h.device_id', 'h.heartrate', 'h.timevalue')
            ->leftJoin('hrtrackers as h', function ($user) {
                $user->on('users.id', '=', 'h.user_id')
                    ->whereRaw('h.id IN (select MAX(h1.id) from hrtrackers as h1 join users as u on u.id = h1.user_id group by u.id)');
            })
            ->orderBy('users.first_name', 'ASC')->get();


        if (!$user->isEmpty()) {
            return response()->json([
                'status' => true,
                'data' => $user,
                'message' => 'Data retrieved successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => true,
                'data' => [],         //"No record Found",
                'message' => 'Data retrieved successfully'
            ], 200);
        }
    }




    /* get historical hr data -past 24 hr */

    public function hrhistory(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required',
            'timevalue' => 'required',
            'timezone' => 'required'
        ]);



        $userid = $request->user_id;
        $timevalue = $request->timevalue;
        $timezone = $request->timezone;




        $from = Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->subHours(24)->toDateTimeString();
        $to = Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->toDateTimeString();

        $hrtracker = Hrtracker::select('heartrate', 'timevalue')->where(
            'created_at',
            '>',
            Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->subHours(24)->toDateTimeString()
        )
            ->where('created_at', '<', Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->toDateTimeString())
            ->orderby('timevalue', 'ASC')->where('user_id', $userid)->get();



        if (!$hrtracker->isEmpty()) {
            return response()->json([
                'status' => true,
                'data' => $hrtracker,
                'message' => 'Data retrieved successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => true,
                'data' => [],
                'message' => 'No values found'
            ], 200);
        }
    }

    /* get historical location  data -past 24 hr */

    public function locationhistory(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required',
            'timevalue' => 'required',
            'timezone' => 'required'
        ]);

        $userid = $request->user_id;
        $timevalue = $request->timevalue;
        $timezone = $request->timezone;

        $hrtracker = Hrtracker::select('heartrate', 'timevalue')->where(
            'created_at',
            '>',
            Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->subHours(24)->toDateTimeString()
        )
            ->where('created_at', '<', Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->toDateTimeString())
            ->orderby('timevalue', 'ASC')->where('user_id', $userid)->get();

        if (!$hrtracker->isEmpty()) {
            return response()->json([
                'status' => true,
                'data' => $hrtracker,
                'message' => 'Data retrieved successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => true,
                'data' => [],
                'message' => 'No values found'
            ], 200);
        }
    }

    public function hrhistory1(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required',
            'timevalue' => 'required',
            'timezone' => 'required'
        ]);



        $userid = $request->user_id;
        $timevalue = $request->timevalue;
        $timezone = $request->timezone;




        $from = Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->subHours(24)->toDateTimeString();
        $to = Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->toDateTimeString();

        $hrtracker = Hrtracker::select('heartrate', 'timevalue')->where(
            'created_at',
            '>',
            Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->subHours(24)->toDateTimeString()
        )
            ->where('created_at', '<', Carbon::parse($timevalue, $timezone)->setTimezone('UTC')->toDateTimeString())
            ->orderby('timevalue', 'ASC')->where('user_id', $userid)->get();



        if (!$hrtracker->isEmpty()) {

            $time = '';
            $hrvaluearray = [];
            $higherrhr = '';
            $resultArray = [];
            $timeintervalTo = Carbon::create($from)->addHours(1)->toDateTimeString();
            foreach ($hrtracker as $key => $value) {

                $data = array(
                    'heartrate' => $value->heartrate,
                    'timevalue' => $value->timevalue
                );

                $time = Carbon::create($value->timevalue)->toDateTimeString();
                //check the time of data is greaterthan or lessthan the time interval (1hr)
            
                if (Carbon::parse($time)->greaterThan($timeintervalTo)) {
                    //if the data is greater than the timeinterval 
                    //check hrarray empty or not

                    if (empty($hrvaluearray)) {
                        //if hrarray empty 
                        //add data to hrarray
                        $hrvaluearray[] =$data;
                    } else {
                        //  if hrarray not empty
                        //  find max and min values from hrarray   if size of hrarray is greater than 2 

                        //check the size of hrarray
                        if(sizeof($hrvaluearray) >= 2 ){
                            //if greater then find min and max values of that timeintervel from hrarray
                            $max = array_keys($hrvaluearray, max($hrvaluearray))[0];
                            $min = array_keys($hrvaluearray, min($hrvaluearray))[0];

                             //extract only the max and min values
                             $selectedArray[]=$hrvaluearray[$min];
                             $selectedArray[]=$hrvaluearray[$max];
                             
                             //merge it to the resultarray
                            $resultArray = array_merge($resultArray, $selectedArray);

                            //sectedarray unset
                            $selectedArray=[];                           
                           
                        }else{
                            //if hrarray size less than 2 take all values of that time interval
                            //merge it to the resultarray
                            $resultArray = array_merge($resultArray, $hrvaluearray);
                       
                        }  
                        
                        //if hrarray not empty 
                        //clear hrarray and push the current time value hrdata
                         $hrvaluearray=[]; 
                         $hrvaluearray[] = $data;               
                    }
                   
                    
                    //$from = $timeintervalTo;
                    //create the new time interval (1 hr duration) from the current data timevalue
                    $timeintervalTo = Carbon::parse($time)->addHours(1)->toDateTimeString();
                   // $timeintervalTo = Carbon::parse($timeintervalTo)->addHours(1)->toDateTimeString();
                    //$hrvaluearray[] = $data;
                } else {

                    //if current data in inside the time interval
                    //add data to the hrarray
                    $hrvaluearray[] = $data;
        
                }
            }

            if(!empty($hrvaluearray)){
                 //check the size of hrarray
                        if(sizeof($hrvaluearray) >= 2 ){
                            //if greater then find min and max values of that timeintervel from hrarray
                            $max = array_keys($hrvaluearray, max($hrvaluearray))[0];
                            $min = array_keys($hrvaluearray, min($hrvaluearray))[0];

                             //extract only the max and min values
                             $selectedArray[]=$hrvaluearray[$min];
                             $selectedArray[]=$hrvaluearray[$max];
                             
                             
                            array_multisort(array_column($selectedArray,'timevalue'));

                             //merge it to the resultarray
                            $resultArray = array_merge($resultArray, $selectedArray);

                            //sectedarray unset
                            $selectedArray=[];                           
                           
                        }else{
                            //if hrarray size less than 2 take all values of that time interval
                            //merge it to the resultarray
                            $resultArray = array_merge($resultArray, $hrvaluearray);
                       
                        }  
            }
           


            return response()->json([
                'status' => true,
                'data' => $resultArray,
                'message' => 'Data retrieved successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => true,
                'data' => [],
                'message' => 'No values found'
            ], 200);
        }
    }
}
