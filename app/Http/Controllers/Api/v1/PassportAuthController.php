<?php


namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class PassportAuthController extends Controller
{
    /**
     * Registration
     */
    public function registration(Request $request)
    {
       
       $userCount = User::where('email', $request->email);
	if ($userCount->count()) {
		return response()->json(['status'=>false,'message'=>'User already exists'],200);
	}
       $validator = Validator::make($request->all(),[
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'mobile_number' => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>'Registration Failed'],200);
        }
        
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'mobile_number'=>$request->mobile_number,
        ]);
       
        $token = $user->createToken('LaravelAuthApp')->accessToken;
 
        $response['id']=$user->id;
        $response['first_name']=$user->first_name;
        $response['last_name']=$user->last_name;
        $response['email']=$user->email;
        $response['mobile_number']=$user->mobile_number;
        $response['token']=$token;
  
 
        return response()->json(['status'=>true,'user' => $response,'message'=>'Registered Successfully'],200);
      
    }
 
    /**
     * Login
     */
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            
            $user['id']=auth()->user()->id;
            $user['first_name']=auth()->user()->first_name;
            $user['last_name']=auth()->user()->last_name;
            $user['email']=auth()->user()->email;
            $user['mobile_number']=auth()->user()->mobile_number;
            $user['token']=$token;
            
            return response()->json(['status'=>true,'user' => $user,'message'=>'Logged in successfully'], 200);
        } else {
            return response()->json(['status'=>false,'message' => 'Unauthorised'], 200);
        }
    }   
}