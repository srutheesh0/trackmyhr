<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\PassportAuthController;
use App\Http\Controllers\Api\v1\HrtrackerController;
//use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('register', [PassportAuthController::class, 'register']);
//Route::post('login', [PassportAuthController::class, 'login']);

Route::group(['prefix' => 'v1'], function () {
  Route::post('registration',      [PassportAuthController::class, 'registration']);
  Route::post('login', [PassportAuthController::class, 'login']);
  
  Route::group(['middleware' => ['auth:api']], function () {
  Route::post('sendhrdata', [HrtrackerController::class, 'sendhrdata']);
  Route::post('gethrdata', [HrtrackerController::class, 'gethrdata']);
    });
  
  Route::post('getuserlist', [HrtrackerController::class, 'getuserlist']); 
  Route::post('getuserdata', [HrtrackerController::class, 'getuserdata']); 
  Route::post('getuserdetails', [HrtrackerController::class, 'getuserdetails']);
  Route::post('hrhistory', [HrtrackerController::class, 'hrhistory']);
  Route::post('locationhistory', [HrtrackerController::class, 'locationhistory']);  

  Route::post('hrhistory1', [HrtrackerController::class, 'hrhistory1']);
  Route::post('locationhistory1', [HrtrackerController::class, 'locationhistory1']);  
      
});


Route::middleware('auth:api')->group(function () {
    Route::resource('posts', PostController::class);
});